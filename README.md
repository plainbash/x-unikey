# unikey
Vietnamese typing for X-windows.

xunikey-fork from https://www.unikey.org. Version x-unikey 1.0.4

While unikey is open source under GNU license, it's not easy to find its source code.

This fork hopes to help community to find the source code easier as a reference.

### Original
#### Unikey Input Method for X-Window
Copyright (C) 2004 Pham Kim Long

This package contains following components:
- ukxim: Unikey XIM (X Input Method) server
- unikey: GUI for ukxim and unikey-gtk
- unikey-gtk: GTK vietnamese input method module

To install: see INSTALL
Manual: see doc/manual


